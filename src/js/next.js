		const cube = document.querySelector('.next__rotate-container-cube');
		const backSide = document.querySelector('.next__rotate-container-cube-side_back');
		const rightArrowCube = document.querySelector('.next__rotate-arrow-right');
		const leftArrowCube = document.querySelector('.next__rotate-arrow-left');
		const topArrowCube = document.querySelector('.next__rotate-arrow-up');
		const bottomArrowCube = document.querySelector('.next__rotate-arrow-down');

		let rotateY = 0;
		let rotateX = 0;
		let rotateFlag = 1;

		function resetX() {
			backSide.style.transform = 'rotateY(180deg) translateZ(9.375rem) rotateZ(0deg)';
			rotateFlag = 1;
			rotateX = 0;
		};

		function resetY () {
			backSide.style.transform = 'rotateY(180deg) translateZ(9.375rem) rotateZ(180deg)';
			rotateFlag = 2;
			rotateY = 0;
		};

		rightArrowCube.addEventListener('click', ()=> {
			if(rotateFlag === 2) {
				resetX();
			}

			rotateY -= 90;
			cube.style.transform = `rotateY(${rotateY}deg)`;
		});

		leftArrowCube.addEventListener('click', (e)=> {
			if(rotateFlag === 2) {
				resetX();
			}

			rotateY += 90;
			cube.style.transform = `rotateY(${rotateY}deg)`;
		});

		topArrowCube.addEventListener('click', ()=> {
			if (rotateFlag === 1) {
				resetY();
			}

			rotateX += 90;
			cube.style.transform = `rotateX(${rotateX}deg)`;
		});

		bottomArrowCube.addEventListener('click', ()=> {
			if (rotateFlag === 1) {
				resetY();
			}
			rotateX -= 90;
			cube.style.transform = `rotateX(${rotateX}deg)`;
		});
