const freeStar = document.querySelectorAll('.free__container-record-quality-star');
const freeButton = document.querySelector('.free__container-record-button');
let freeValue = document.querySelector('.free__container-record-quality-value');
let freeStarCounter = 0;


for(let i = 0; i < freeStar.length; i++) {

	freeStar[i].addEventListener('click', ()=> {
		let serialNumber =  i;

		for(let i = 0;  i < freeStar.length; i++) {

			if(i <= serialNumber) {
				freeStar[i].style.color = 'yellow';
			} else {
				freeStar[i].style.color = '';
			}
		}
	})

	freeStar[i].addEventListener('mouseover', (e)=> {
		let serialNumber =  i;

		for(let i = 0; i < freeStar.length; i++) {
			if(i <= serialNumber) {
				freeStar[i].style.color = 'gold';
			} else {
				freeStar[i].style.color = '';
			}
		}
	})
}

freeButton.addEventListener('click', ()=> {
	freeValue.innerText = `(${Math.floor(Math.random() * 999)}K)`
})