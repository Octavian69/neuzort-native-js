
	const socialContainer = document.querySelector('.social__container');
	const popularSection = document.querySelector('.social__head-gallery-popular');
	const recentSection = document.querySelector('.social__head-gallery-recent');
	const socialButton = document.querySelector('.social__button');
	let socialContainerBox = document.querySelectorAll('.social__container-box');

	let noActive = recentSection;

	function sendSocial (url) {
		
		return new Promise((resolve, reject)=> {

			let xhr = new XMLHttpRequest();

			xhr.open('GET', url);

			xhr.addEventListener('load', ()=> {
				resolve(xhr.response);
			})

			xhr.addEventListener('error',()=> {
				reject('File not found');
			})

			xhr.send();
		})
	}

	const popular = './popular.html';
	const recent  = './recent.html';
	const mini  = './mini.html';

	function colorActive (elem) {
		 elem.style.color = '#b7b7b7';

		 if(noActive && noActive != elem) {
		 	noActive.style.color = '#333333';
		 }
	}


	popularSection.addEventListener('click', function() {
		sendSocial(popular).then(
			(response)=> {
				socialContainer.innerHTML = response;
				colorActive(this);
				noActive = this;
			},

			(error)=> {
				console.error(new Error(error));
			}
		)
	})

	recentSection.addEventListener('click', function() {
		sendSocial(recent).then(
			(response)=>{
				socialContainer.innerHTML = response;
				colorActive(this);
				noActive = this;
			},

		    (error)=> {
		    	console.error(new Error(error));
		    }
	    )
	})


	socialButton.addEventListener('click', function () {
		sendSocial(mini).then(
			(response)=> {
				socialContainer.innerHTML = response;
			},

			(error)=>{
				console.error(new Error(error));
			}
		)
	})

