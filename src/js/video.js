

	const playVideo = document.querySelector('.video__expand-box-play'),
		  playText = document.querySelector('.video__expand-text'),
		  playBox = document.querySelector('.video__expand-box'),
		  video = document.querySelector('.video__expand-block');

	  function elementOpacity (elem) {
	  		elem.style.opacity = '0';
	  }

	  function reset (elem) {
	  		elem.style.opacity = '1';
	  }

	  playVideo.addEventListener('click', ()=> {
	  		elementOpacity(playBox);
	  		elementOpacity(playText);
	  		video.style.display = 'block';

	  		setTimeout(()=> {
	  			video.style.opacity = '1';
	  		}, 1)
	  })

video.addEventListener('pause', ()=> {
	// reset(playBox);
	// reset(playText);
	console.log(123);
})