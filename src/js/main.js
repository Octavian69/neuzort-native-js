;(()=> {
	const cursorContainer = document.querySelector('.main__container-load-cursor');
	const cursor = document.querySelector('.main__container-load-cursor-switch');

	cursorContainer.addEventListener('click',()=> {
		cursor.classList.toggle('toggle-switch');
		cursorContainer.classList.toggle('toggle-shadow-cursor');
	})
})()