;(()=> {
	const leftArrow = document.querySelector('.review__slider-left'),
		  rightArrow = document.querySelector('.review__slider-right'),
		  boxStar = document.querySelector('.comment-quality'),
		  stars = document.querySelectorAll('.comment-quality-star'),
		  slideStars = document.querySelectorAll('.review__slider-box-quality-star'),
		  addComment = document.querySelector('.review__slider-add-comment'),
		  commentBlur = document.querySelector('.comment-bg');
		  commentOpacity = document.querySelector('.comment-bg-blur');
		  commentForm = document.querySelector('.comment'),
		  commentPhotoContainer = document.querySelector('.comment-photo'),
		  commentPhotoText = document.querySelector('.comment-photo-text'),
		  sliderContainer = document.querySelector('.review__slider'),
		  sliderBox = document.querySelectorAll('.review__slider-box'),
		  sliderBoxWrapper = document.querySelector('.review__slider-wrapper'),
		  commentName = document.querySelector('#commentName'),
		  commentCompany = document.querySelector('#commentCompany'),
		  commentText = document.querySelector('#commentText'),
		  commentError = document.querySelectorAll('.comment-error');

		let yellowStar = 0;

	  	class Interval {
		  	constructor() {
		  		this.step;
		  		this.func;
		  		this.counter = 0;
		  		this.leftStep = this.leftStep.bind(this);
		  		this.rightStep = this.rightStep.bind(this);
		  	}

		  	leftStep () {

			  	if(this.counter == 0) {
			  		return false;
			  	}

			  	sliderBox[this.counter].previousSibling.style.marginLeft = '0%';
			  	this.counter--;
		  	}

		  	rightStep () {

		  		if(this.counter === sliderBox.length - 1) {
		  			return false;
		  		}

			  	sliderBox[this.counter].style.marginLeft = '-100%'
			  	this.counter++;
		  }



		  	right() {
		  		this.func = this.right;
		  		this.removeInterval(this.step);
		  		this.step = setInterval(()=> {
		  			if(this.counter < sliderBox.length - 1) {
		  				this.rightStep();
		  			} else {
		  				this.left();
		  			}
		  		}, 1500)
		  	}

		  	left() {
		  		this.func = this.left;
		  		this.removeInterval(this.step);
		  		this.step = setInterval(()=> {
		  			if(this.counter > 0) {
		  				this.leftStep();
		  				
		  			} else {
		  				this.removeInterval(this.step);
		  				this.right();
		  			}
		  		}, 1500)
		  	}

		  	nameStep() {
		  		this.func();
		  	}

		  	removeInterval(interval) {
		  		clearInterval(interval);
		  	}
	  }

	  let sliderInterval = new Interval();

	  sliderInterval.right();



	leftArrow.addEventListener('click', sliderInterval.leftStep);

	rightArrow.addEventListener('click', sliderInterval.rightStep);



	  sliderContainer.addEventListener('mouseover', (e)=> {
			sliderInterval.removeInterval();
	  })

	  sliderContainer.addEventListener('mouseout', (e)=> {
			sliderInterval.nameStep();
	  })	


		addComment.addEventListener('click', ()=> {
			commentBlur.style.display = 'block';

			setTimeout(()=> {
				commentOpacity.style.opacity = '0.8';
				commentForm.style.left = '50%';
				commentForm.style.opacity = '1';
			},100)
			
		})

	  stars.forEach((item, i) => {
	  		item.addEventListener('mouseover', ()=> {
	  			let count = i;
	  			
	  			for(let i = 0; i < stars.length; i++) {
	  				if (i <= count) {
	  					stars[i].style.color = 'yellow';
	  					yellowStar = i;
	  				} else {
	  					stars[i].style.color = '';
	  				}
	  			} 
	  		})
	  });


	  commentPhoto.addEventListener('change', (e)=> {
	  		let file = e.target.files[0],
	  			reader = new FileReader(),
	  			result;

 			reader.addEventListener('load', (e)=> {
  				result = e.target.result;
  				commentPhotoContainer.style.background = `url('${result}') no-repeat center`;
  				commentPhotoContainer.style.backgroundSize = 'cover';
  				commentPhotoText.style.display = 'none';
  			})

  			if(file) {
  				reader.readAsDataURL(file);
  			}
	  })


	  function hideForm () {
			commentOpacity.style.opacity = '0';
			commentForm.style.left = '-100%';
			commentForm.style.opacity = '0';


			setTimeout(()=> {
				commentBlur.style.display = 'none';
				sliderInterval.nameStep();
			},1000)
	  }


	  

	  function cloneSlide() {
	  		let div = sliderBox[0].cloneNode(true);
	  		let divStars = div.querySelectorAll('.review__slider-box-quality-star');
			

	  		sliderBoxWrapper.appendChild(div);
	  		sliderBox = document.querySelectorAll('.review__slider-box');
	  		div.style.marginLeft = '0%';


	  		for(let i = 0; i <= yellowStar; i++) {
	  			divStars[i].style.color = 'yellow';
	  		}

	  		div.querySelector('.review__slider-box-id-name').innerText = commentName.value + ' ';
	  		div.querySelector('.review__slider-box-id-company').innerText = commentCompany.value;
	  		div.querySelector('.review__slider-box-text').innerText = commentText.value;

	  		if(commentPhotoContainer.style.backgroundImage) {
		  		div.querySelector('.review__slider-box-img').style.background = commentPhotoContainer.style.background;
	  			div.querySelector('.review__slider-box-img').style.backgroundSize = 'cover';
	  		}
	  }


	  commentForm.addEventListener('submit', (e)=> {

	  	function error (name, num) {
	  			e.preventDefault();
	  			name.focus();
	  			name.style.borderImage = 'linear-gradient(to right, transparent 0%, tomato 100%)';
	  			name.style.borderImageSlice = '1';
	  			commentError[num].style.display = 'inline-block';
	  	}

	  	function clear (name, num) {
  			e.preventDefault();
  			name.style.borderImage = '';
  			name.style.borderImageSlice = '';
  			commentError[num].style.display = '';
  			name.value = '';
	  	}

	  	if(commentName.value.match(/^[ ]+$/)) {
	  		error(commentName, 0);

	  	}

	  	if(commentCompany.value.match(/^[ ]+$/)) {
	  		error(commentCompany, 1);
	  	}



	  	if(commentText.value.match(/^[ ]+$/)) {
	  		error(commentText, 2);
	  	}



	  	switch(true) {

	  		case commentName.value.length === 0:
	  			error(commentName, 0)
	  			break;

  			case commentCompany.value.length === 0:
	  			error(commentCompany, 1);
	  			break;

  			case commentText.value.length === 0:
	  			error(commentText, 2);
	  			break;	

	  		default: 
	  			e.preventDefault();
	  			hideForm();
	  			cloneSlide();
				commentPhotoContainer.style.background = '';
				commentPhotoText.style.display = '';
				clear(commentName, 0);
				clear(commentCompany, 1);
				clear(commentText, 2);
				
	  	}

  	})

	  window.addEventListener('keyup', (e)=> {
	  		if (e.keyCode === 27 && commentBlur.style.display === 'block') {
				hideForm();
	  		}
	  })

	  window.addEventListener('mousewheel', (e)=> {
		  	if(commentBlur.style.display === 'block') {
		  		e.preventDefault();
		  	}
	  })
	
})();




function methodName (x) {
	let oneNum = x;

	function  stepGo (step) {
		return oneNum += step;
	}

	stepGo.clear = function () {
		oneNum = x;
		console.log(oneNum);
	}

	return stepGo;
}


let oneStep = methodName(5);

