const footerItem = document.querySelectorAll('.footer__container-menu-item');
const listShow = document.querySelectorAll('[data-set-show-element]');
const listId = document.querySelectorAll('[data-id]');
const footerShow = document.querySelector('.footer__show-container');
const footerBg = document.querySelector('.footer__show');

let showElement;

function showFooterElement (element) {
	footerBg.style.display = 'flex';
	footerBg.style.opacity = '1';
	document.body.style.overflow = 'hidden';

	setTimeout((e)=> {
		element.style.margin = '0';
	}, 100)
}

function hiddenFooterElement (element) {
	element.style.margin = '';
	footerBg.style.opacity = '0';
	document.body.style.overflow = '';

	setTimeout((e)=> {
		footerBg.style.display = '';
	}, 900)
}

footerBg.addEventListener('click', (e)=> {
	if(e.target.className === 'footer__show' || e.target.className === 'material-icons footer__show-close') {
		hiddenFooterElement(showElement);
	}
	
})

for(let item of listShow) {

	item.addEventListener('click', function () {
		let id = footerShow.querySelector(`[data-${item.dataset.setShowElement}]`);
		showElement = id;
		showFooterElement(id);
	})
}

let thisItem;

for(item of footerItem) {

	item.addEventListener('mouseover', function() {
		if(thisItem) {
			thisItem.style.height = '12%';
		}

		if(thisItem != footerItem[0]) {
			footerItem[0].style.height = '12%';
		}
		
		thisItem = this;
		this.style.height = '100%'
	})
}


const footerSection = document.querySelector('.footer'),
	  footerEnd = document.querySelector('.footer__container-end');


function hrScroll() {
	 let valueScroll = 0;

	 function goScroll(e) {
	 	 let size = window.pageYOffset + window.innerHeight;

	 	 if(valueScroll <= (-100)) {
	 	 	window.removeEventListener('mousewheel', goScroll);
	 	 	console.log(123);
	 	 }


 	 	 if( size >= document.body.clientHeight) {
			footerSection.scrollIntoView();
			valueScroll -= 10;
			e.preventDefault();
			footerEnd.style.left = valueScroll + '%';
		 }
	 }

	return goScroll;
}

let goScroll = hrScroll();

window.addEventListener('mousewheel', goScroll);


