const allImg = document.querySelectorAll('.interface__container-phone-img');
const interfaceContainer = document.querySelector('.interface');
let left = document.querySelector('.interface__page-list-left');
let right = document.querySelector('.interface__page-list-right');

let leftStep = -90;
let rightStep = 90;

function sortImg () {

	let positive = 0; 
	let negative = -90;
	
	for(let i = 0; i < allImg.length; i++) {
		if(i <= 2 ) {
			allImg[i].style.left = positive + 4 + '%';
			positive += 90;
		} else if (i > 2) {
			allImg[i].style.left = negative + 4 + '%';
			negative += -90;
		}
	}
}


function callSortImg () {
	let windowHeight = interfaceContainer.offsetTop - (window.innerHeight / 4);

	if (window.pageYOffset > windowHeight) {
		sortImg();
		window.removeEventListener('scroll', callSortImg);
	}


};

window.addEventListener('scroll', callSortImg);


left.addEventListener('click', ()=> {

	if(allImg[2].style.left == '4%') {
		return false;
	}

	for(let i = 0; i < allImg.length; i++) {
		let leftValue = +allImg[i].style.left.slice(0, allImg[i].style.left.indexOf('p'));
		allImg[i].style.left = leftValue + leftStep + '%';
	}
})

left.addEventListener('mousedown', ()=> {
	left.style.transform = 'scale(0.7)';
})

left.addEventListener('mouseup', ()=> {
	left.style.transform = '';
})


right.addEventListener('click', ()=> {

	if(allImg[allImg.length - 1].style.left == '4%') {
		return false;
	}

	for(let i = 0; i < allImg.length; i++) {
		let rightValue = +allImg[i].style.left.slice(0, allImg[i].style.left.indexOf('p'));
		allImg[i].style.left = rightValue + rightStep + '%';
	}
})

right.addEventListener('mousedown', ()=> {
	right.style.transform = 'scale(0.7)';
})

right.addEventListener('mouseup', ()=> {
	right.style.transform = '';
})	



